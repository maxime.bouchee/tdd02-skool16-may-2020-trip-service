'use strict';

const expect = require('chai').expect;
const TripService = require('../src/TripService');

describe('TripService', () => {

  it('should not fail', () => {
    // Given
    const tripService = new TripService();

    // When
    const trips = tripService.getTripsByUser();

    // Then
    expect(trips).to.equal('not an exception');
  });

});
